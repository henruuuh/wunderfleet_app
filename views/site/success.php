<?php
   use yii\helpers\Url;
?>
<div class="container">
   <div class="row">
      <div class="col-md-12">
         <div class="box">
            <div class="text-center" style="margin-bottom: 30px">
               <img src="http://www.dovera.sk/media/!media/!media/micro_2015_cukrovka/img/svg/icon--checkmark.svg" height="80px" width="80px" class="text-center" />
            </div>
            <h1 class="text-center"> Thank you! </h1>
            <h3 class="text-center"> Your registration is successful! </h3>
            <br><br>
            <h3 class="text-center"> Your Payment Data ID is:  </h3>
            <strong><h3 class="text-center" style="font-size: 15px"> <?php echo Yii::$app->session->get('paymentdataid');?> </h3></strong>
            <div class="col-md-6 offset-3">
               <center>
               <br><br><button type="submit" id="submitBtn" class="btn custom-button"> Back to Registration</button></center>
               </center>
            </div>
         </div>
      </div>
   </div>
</div>

<script type="text/javascript">
   const url = '<?php echo Url::to(['site/restart']);?>';

   $(document).ready(function(){
      $('#submitBtn').on('click', function() {
         $.ajax({
             type:'GET',
             url: url,
             success:function(result){
             },
              error: function(xhr, ajaxOptions, thrownError){
                 console.log(xhr.status);
             },
         }); 
      });
   });
</script>
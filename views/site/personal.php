<?php
   use yii\helpers\Url;
?>
<div class="container">
   <div class="row">
      <div class="col-md-8 offset-md-2 nopadding">
         <div class="col-md-4">
            <div class="titlebox">
               <h4> PERSONAL</h4>
            </div>
         </div>
         <div class="box">
            <form action="<?php echo Url::to(['site/handle-personal']);?>" method="POST">
               <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
               <div class="form-group col-md-6 offset-md-3">
                  <label for="first-name"><b>First Name</b></label>
                  <input type="text" class="form-control inputbox" name="first-name" placeholder="Enter your First Name" pattern="[a-z A-Z]{2,30}" title="First Name must include a minimum of 2 and a maximum of 30 alphabetical characters only." required>
               </div>
               <div class="form-group col-md-6 offset-md-3">
                  <label for="last-name"><b>Last Name</b></label>
                  <input type="text" class="form-control inputbox" name="last-name" placeholder="Enter your Last Name" pattern="[a-z A-Z]{2,30}" title="Last Name must include a minimum of 2 and a maximum of 30 alphabetical characters only." required>
               </div>
               <div class="form-group col-md-6 offset-md-3">
                  <label for="contact-number"><b>Contact Number</b></label>
                  <input type="text" class="form-control inputbox" name="contact-number" placeholder="Enter your Contact #" pattern="[-0-9()]{6,}" title="Contact number should contain numbers and parentheses only, with a minimum of 6 characters." required>
               </div>
               <div class="col-md-6 offset-3">
                  <center>
                  <br><br><button type="submit" class="btn custom-button"><strong>NEXT</strong></button>
                  </center>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
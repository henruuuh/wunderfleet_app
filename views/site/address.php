<?php
   use yii\helpers\Url;
?>
<div class="container">
   <div class="row">
      <div class="col-md-8 offset-md-2 nopadding">
         <div class="col-md-4">
            <div class="titlebox">
               <h4> ADDRESS </h4>
            </div>
         </div>
         <div class="box">
            <form action="<?php echo Url::to(['site/handle-address']);?>" method="POST">
               <input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
               <div class="form-group col-md-6 offset-md-3">
                  <label for="street-name"><b>Street</b></label>
                  <input type="text" class="form-control inputbox" name="street-name" placeholder="Enter your Street Name" pattern="[a-z A-Z-0-9#,.]{2,30}" title="Street Name must include a minimum of 2 and a maximum of 30 alphanumeric characters, periods, commas, and hashes only." required>
               </div>
               <div class="form-group col-md-6 offset-md-3">
                  <label for="house-number"><b>House Number</b></label>
                  <input type="text" class="form-control inputbox" name="house-number" placeholder="Enter your House Number" pattern="[0-9]{1,10}" title="House number must include a minimum of 2 and a maximum of 10 numeric characters only." required>
               </div>
               <div class="form-group col-md-6 offset-md-3">
                  <label for="city-name"><b>City</b></label>
                  <input type="text" class="form-control inputbox" name="city-name" placeholder="Enter your City" pattern="[a-z A-Z]{2,30}" title="City must include a minimum of 2 and a maximum of 30 alphabetical characters only." required>
               </div>
               <div class="form-group col-md-6 offset-md-3">
                  <label for="zip-code"><b>Zip Code</b></label>
                  <input type="text" class="form-control inputbox" name="zip-code" placeholder="Enter your Zip Code" pattern="[0-9]{3,10}" title="House number must include a minimum of 3 and a maximum of 10 numeric characters only." required>
               </div>
               <div class="col-md-6 offset-3">
                  <center>
                     <br><br><button type="submit" class="btn custom-button"><strong>NEXT</strong></button>
                  </center>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
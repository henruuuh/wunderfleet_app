<?php
	use yii\helpers\Url;
?>
<div class="container">
	<div class="row">
		<div class="col-md-8 offset-md-2 nopadding">
			<div class="col-md-4">
				<div class="titlebox">
					<h4> PAYMENT </h4>
				</div>
			</div>
			<div class="box">
				<form action="<?php echo Url::to(['site/handle-payment']);?>" method="POST">
               	<input type="hidden" name="_csrf" value="<?=Yii::$app->request->getCsrfToken()?>" />
					<div class="form-group col-md-6 offset-md-3">
						<label for="account-owner"><b>Account Owner</b></label>
						<input type="text" class="form-control inputbox" id="account-owner" name="account-owner" placeholder="Enter the Account Owner" pattern="[a-z A-Z]{2,30}" title="Account Owner must include a minimum of 2 and a maximum of 30 alphabetical characters only" required>
					</div>
					<div class="form-group col-md-6 offset-md-3">
						<label for="iban"><b>IBAN</b></label>
						<input type="text" class="form-control inputbox" id="iban" name="iban" placeholder="Enter the IBAN" pattern="[0-9 A-Z]{1,34}" title="IBAN must include a minimum of 1 and a maximum of 34 alphanumeric characters only." required>
					</div>
					<div class="col-md-6 offset-3">
						<center>
						<br><br><button type="submit" id="submitBtn" class="btn custom-button">SUBMIT</button></center>
						</center>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
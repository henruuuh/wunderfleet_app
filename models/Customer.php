<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $firstname
 * @property string $lastname
 * @property string $contactnumber
 * @property string $streetname
 * @property int $housenumber
 * @property string $cityname
 * @property int $zipcode
 * @property string $iban
 * @property string $accountowner
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'contactnumber', 'streetname', 'housenumber', 'cityname', 'zipcode', 'iban', 'accountowner'], 'required'],
            [['housenumber', 'zipcode'], 'integer'],
            [['firstname', 'lastname', 'contactnumber', 'streetname', 'cityname', 'iban', 'accountowner'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'contactnumber' => 'Contactnumber',
            'streetname' => 'Streetname',
            'housenumber' => 'Housenumber',
            'cityname' => 'Cityname',
            'zipcode' => 'Zipcode',
            'iban' => 'Iban',
            'accountowner' => 'Accountowner',
        ];
    }
}

<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\Customer;

class SiteController extends Controller
{
    public function actionIndex()
    {	
    	$session = Yii::$app->session;  

    	if($session->has('personal_done'))
    	{
    		$this->redirect('address');
    	}
    	else
    	{
        	return $this->render('personal');	
    	}
    }

    public function actionHandlePersonal()
    {
    	$session = Yii::$app->session;  
        $session->set('personal_done', '1');

        $request = Yii::$app->request;

        $firstname = $request->post('first-name');
        $lastname = $request->post('last-name');
        $contactnumber = $request->post('contact-number');

        $session->set('firstname', $firstname);
        $session->set('lastname', $lastname);
        $session->set('contactnumber', $contactnumber);

        $this->redirect('address');
    }

    public function actionAddress()
    {	
    	$session = Yii::$app->session; 
    	if($session->has('address_done')) 
    	{
    		$this->redirect('payment');
        } 
        else if($session->has('personal_done')) 
        {
			return $this->render('address');
        }
        else
        {
        	$this->redirect('/');
        }
    }

    public function actionHandleAddress()
    {
    	$session = Yii::$app->session;  
        $session->set('address_done', '1');

        $request = Yii::$app->request;

        $streetname = $request->post('street-name');
        $housenumber = $request->post('house-number');
        $cityname = $request->post('city-name');
        $zipcode = $request->post('zip-code');

        $session->set('streetname', $streetname);
        $session->set('housenumber', $housenumber);
        $session->set('cityname', $cityname);
        $session->set('zipcode', $zipcode);

        $this->redirect('payment');
    }

    public function actionPayment()
    {
    	$session = Yii::$app->session; 
    	if($session->has('payment_done')) 
    	{
    		$this->redirect('success');
        } 
        else if($session->has('personal_done') && $session->has('address_done'))
        {
        	return $this->render('payment');
        }
        else
        {
        	$this->redirect('/');
        }
    }

    public function actionHandlePayment()
    {
    	$session = Yii::$app->session;  
    	$request = Yii::$app->request;

        $session->set('payment_done', '1');

        $model = new Customer;
        $model->firstname = $session->get('firstname');
        $model->lastname = $session->get('lastname');
        $model->contactnumber = $session->get('contactnumber');
        $model->streetname = $session->get('streetname');
        $model->housenumber = $session->get('housenumber');
        $model->cityname = $session->get('cityname');
        $model->zipcode = $session->get('zipcode');
        $model->iban = $request->post('iban');
        $model->accountowner = $request->post('account-owner');
        $model->save();

        $id = $model->find()->max('id');
        $data = array(
	    	'customerId' => $id, 
	    	'iban' => $request->post('iban'),
	    	'owner' => $request->post('account-owner')
	  	);  

		$json_data = json_encode($data);

		$url = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$json_data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$result  = curl_exec($ch);

		curl_close($ch);

		$decode_data = json_decode($result);
		$current = Customer::findOne($id);
		$current->paymentdataid = $decode_data->paymentDataId;
		$current->save();

		$session->set('paymentdataid', $decode_data->paymentDataId);

    	$this->redirect('success');
    }

    public function actionSuccess()
    {
    	$session = Yii::$app->session; 
    	if($session->has('personal_done') && $session->has('address_done') && $session->has('payment_done'))
    	{
        	return $this->render('success');
    	}
    	else
    	{
    		$this->redirect('/');
    	}
    }

    public function actionRestart()
    {
    	$session = Yii::$app->session; 
    	$session->destroy();
        $this->redirect('/');
    }
}
